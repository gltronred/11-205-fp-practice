
deleteEven = filter odd

squaresOf :: [Int] -> [Int]
squaresOf [] = []
squaresOf (x:xs) = x*x : squaresOf xs

isEvens :: [Int] -> [Bool]
isEvens [] = []
isEvens (x:xs) = (if even x then True else False) : isEvens xs

myMap :: (a -> b) -> [a] -> [b]
myMap f [] = []
myMap f (x:xs) = f x : myMap f xs

squaresOf' = myMap (\x -> x*x)
isEvens' = myMap even

mySum :: [Int] -> Int
mySum [] = 0
mySum (x:xs) = x + mySum xs

myAll :: [Bool] -> Bool
myAll [] = True
myAll (x:xs) = x && myAll xs

myFun :: [Int] -> [[Int]]
myFun [] = []
myFun (x:xs) = (if even x then [x] else [0]) : myFun xs

myFun' = myFoldr (\x res -> (if even x then [x] else [0]) : res) []
myFun'' = myFoldl (\res x -> (if even x then [x] else [0]) : res) []

myFoldr :: (a -> b ->b) -> b -> [a] -> b
myFoldr f b [] = b
myFoldr f b (x:xs) = f x $ myFoldr f b xs

myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f a [] = a
myFoldl f a (x:xs) = myFoldl f (f a x) xs

-- foldl1, foldr1

g :: (Integer, Char)
g = (1,'a')

-- fst, snd

g' :: (Integer, Char, (Integer,Char))
g' = (1,'a', (2,'b'))

