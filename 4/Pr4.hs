
data IntList = Nil
             | Cons Int IntList
               deriving (Show)

g :: IntList
g = Cons 3 (Cons 2 (Cons 1 Nil))

mySum :: IntList -> Int
mySum Nil = 0
mySum (Cons x xs) = x + mySum xs

deleteEvens :: IntList -> IntList
deleteEvens Nil = Nil
deleteEvens (Cons x xs) | even x = deleteEvens xs
                        | otherwise = Cons x $ deleteEvens xs

data SumExpr = Val Int
             | Sum SumExpr SumExpr
               deriving (Show)

eval :: SumExpr -> Int
eval (Val x) = x
eval (Sum e1 e2) = eval e1 + eval e2

data List a = Empty
            | List a (List a)

deleteMod3 :: IntList -> IntList
deleteMod3 Nil = Nil
deleteMod3 (Cons x xs) | x`mod`3 == 0 = deleteMod3 xs
                       | otherwise    = Cons x $ deleteMod3 xs

deleteIf :: (Int -> Bool) -> IntList -> IntList
deleteIf f Nil = Nil
deleteIf f (Cons x xs) | f x = deleteIf f xs
                       | otherwise = Cons x $ deleteIf f xs

deleteEvens' = deleteIf even
deleteMod3' = deleteIf (\x -> x`mod`3 == 0)

