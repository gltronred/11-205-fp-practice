
-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt = undefined
             
-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt = undefined
